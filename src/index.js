import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { BrowserRouter } from 'react-router-dom';
import FavProvider from "./context/FavContext"
import ProductProvider from './context/ProductContext';
import CartProvider from "./context/CartContext"

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <ProductProvider>
  <CartProvider>
  <FavProvider>
  <BrowserRouter>
  <React.StrictMode>
   
    <App />

  </React.StrictMode>
  </BrowserRouter>
  </FavProvider>
  </CartProvider>
  </ProductProvider>
);

