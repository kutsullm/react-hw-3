import { createContext,useContext,useState } from "react";

export const CartContext = createContext()

export const useCart = ()=>useContext(CartContext)

export default function CartProvider({children}){
    const [cart,setCart] = useState(JSON.parse(localStorage.getItem("cart")) || [])
    return (
        <CartContext.Provider value={{cart,setCart}} >{children}</CartContext.Provider>
    )
}