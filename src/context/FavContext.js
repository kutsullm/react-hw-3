import { createContext,useContext,useState } from "react";

export const FavContext = createContext()

export const useFav = ()=>useContext(FavContext)

export default function FavProvider({children}){
    const [fav,setFav] = useState(JSON.parse(localStorage.getItem("fav")) || [])
    return (
        <FavContext.Provider value={{fav,setFav}} >{children}</FavContext.Provider>
    )
}