import React, { useState, useEffect } from 'react'
import ProductList from '../../components/ProductList'
import Modal from '../../components/Modal'
import { useFav } from '../../context/FavContext'
import { useCart } from "../../context/CartContext"
import { useProduct } from '../../context/ProductContext'



export const HomePage = () => {
  const { fav, setFav } = useFav()
  const { cart, setCart } = useCart()
  const { products, setProducts } = useProduct()
  const [favorite, setFavorite] = useState(fav)
  const [cartInStorage, setCartInStorage] = useState(cart)
  const [isOpenModal, setModalOpen] = useState(false)
  const [currentProductId, setCurrentProductId] = useState(null)

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cartInStorage))
  }, [cartInStorage])

  useEffect(() => {
    localStorage.setItem("fav", JSON.stringify(favorite))
  }, [favorite])

  function addToCart(id) {
    const findedId = cart.find((item) => item.id === id)

    if (!findedId) {
      const inCart = [...cart, { id, counter: 1 }]
      setCart(inCart)
      setCartInStorage(inCart)

    }
    if (findedId) {
      let counter = findedId.counter + 1
      const findedIndex = cart.findIndex((item) => item.id === id)
      const inCart = cart.toSpliced(findedIndex, 1, { id, counter })
      setCart(inCart)
      setCartInStorage(inCart)

    }
    setModalOpen(false)
    setCurrentProductId(null)




  }
  function toggleInFavorite(id) {

    const findedIdinFav = fav.find((item) => item === id)
    if (!findedIdinFav) {
      const favorite = [...fav, id]
      setFav(favorite)
      setFavorite(favorite)
      const productWithFavorite = products.map((item) => {
        if (item.id === id) {
          item.isInFav = true
          return item
        }
        return item
      })
      setProducts(productWithFavorite)

    }
    if (findedIdinFav) {

      const favorite = fav.filter((item) => item !== id)
      const productsWithFavorite = products.map((item) => {
        if (item.id === id) {
          item.isInFav = false

          return item
        }
        return item
      })
      setFav(favorite)
      setFavorite(favorite)
      setProducts(productsWithFavorite)
 
    }

  }
  function hideModal() {
    setModalOpen(false)
    setCurrentProductId(null)
  }
  function showModal(id) {
    setModalOpen(true)
    setCurrentProductId(id)
  }
  return (
    <><ProductList toggleInFavorite={toggleInFavorite} showModal={showModal} products={products} />

      {isOpenModal && <Modal hideModal={hideModal} onSubmit={addToCart} currentProductId={currentProductId} modalName="addToCart" />}
    </>
  )
}
