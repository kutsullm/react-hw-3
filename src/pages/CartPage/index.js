import React, { useEffect, useState } from 'react'
import ProductList from '../../components/ProductList'
import { useCart } from "../../context/CartContext"
import { useProduct } from '../../context/ProductContext'
import { Cart } from '../../components/cart'
import Modal from '../../components/Modal'
import { useFav } from '../../context/FavContext'

export const CartPage = () => {

 
  const { fav, setFav } = useFav()
  const { products, setProducts } = useProduct()
    const { cart, setCart } = useCart()
     const [favorite, setFavorite] = useState(fav)
   const [cartInStorage, setCartInStorage] = useState(cart) 
    const [currentProductId, setCurrentProductId] = useState(null)
      const [productsInCart, setProductsInCart] = useState([])
      const [isModalOpen, setModalOpen] = useState(false)
 

  

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cartInStorage))
  }, [cartInStorage])
  useEffect(() => {
    localStorage.setItem("fav", JSON.stringify(favorite))
  }, [favorite])
  useEffect(() => {
    function getProducts() {

      const idArray = cart.map((item) => item.id)
      const cartFiltered = products.filter((item) => {

        if (idArray.includes(item.id)) {
          return item
        }

      })

      const cartProducts = cartFiltered.map((item) => {
        const finded = cart.find((elem) => elem.id === item.id)
        return { ...item, ...finded }

      })
      setProductsInCart(cartProducts)
    }
    getProducts()

  }, [products])
  function removeFromCart(id) {
    const filtered = productsInCart.filter((item) => item.id !== id)
    setProductsInCart(filtered)
    const secondaryFiltered = cart.filter((item) => item.id !== id)
    setCart(secondaryFiltered)
    setCartInStorage(secondaryFiltered)
    hideModal()
  }
  function toggleInFavorite(id) {
    const findedIdinFav = fav.find((item) => item === id)
    if (!findedIdinFav) {
      const favorite = [...fav, id]
      setFav(favorite)
      setFavorite(favorite)
      const productWithFavorite = products.map((item) => {
        if (item.id === id) {
          item.isInFav = true
          return item
        }
        return item
      })
      setProducts(productWithFavorite)
      // setFav(favorite)

    }
    if (findedIdinFav) {

      const favorite = fav.filter((item) => item !== id)
      const productsWithFavorite = products.map((item) => {
        if (item.id === id) {
          item.isInFav = false

          return item
        }
        return item
      })
      setFav(favorite)
      setFavorite(favorite)
      setProducts(productsWithFavorite)
      

    }


  }
  function hideModal() {
    setModalOpen(false)
    setCurrentProductId(null)
  }
  function showModal(id) {
    setModalOpen(true)
    setCurrentProductId(id)
  }

  return (

    <>{productsInCart.map((product) => {
      return <Cart toggleInFavorite={toggleInFavorite} showModal={showModal} key={product.id}{...product} />
    })}
      {isModalOpen && <Modal hideModal={hideModal} onSubmit={removeFromCart} currentProductId={currentProductId} modalName="addToCart" />}</>

  )
}
