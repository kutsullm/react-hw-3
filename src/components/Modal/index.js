import Button from "../Button";

import { StyledModal,ModalBckg,ModalContent,StyledParagraph,StyledHeader,StyledH,StyledButton } from "./styled";
import PropTypes from "prop-types";
import { configModal } from "./configModal";
const Modal =({modalName,onSubmit,currentProductId,hideModal}) =>{
 
    const { title, text,hasCloseButton } = configModal[modalName];
    const optionalCeneterd = {
      'justifyContent': hasCloseButton ? 'space-between' : 'center',
    }
    return (
      <ModalBckg onClick={() => hideModal()}>
        <StyledModal onClick={e => e.stopPropagation()}>
          <StyledHeader style={optionalCeneterd}>
            <StyledH>{title}</StyledH>
            {hasCloseButton && <StyledButton onClick={() => hideModal()}>&#10761;</StyledButton>}
          </StyledHeader>
          <ModalContent>
           <StyledParagraph>{text}</StyledParagraph>
            <div>
            <Button text='Ok' backgroundColor='#008CBA' onClick={() => {
              onSubmit(currentProductId)
            }} />,
            <Button text='Cancel' backgroundColor='#e7e7e7' onClick={() => {
              hideModal();
            }} />
            </div>
          </ModalContent>
        </StyledModal>
      </ModalBckg>
    )
  }

Modal.propTypes = {
  title: PropTypes.string,
  text: PropTypes.string,
  id: PropTypes.number,
  hideFn: PropTypes.func,
  onSubmit: PropTypes.func,
  hasCloseButton: PropTypes.bool,
}
export default Modal;