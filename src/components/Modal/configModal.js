
export const configModal = {
addToCart:{
    id: 0,
    title: 'Do you want to delete this file?',
    text: 'Once you delete this file, it won’t be possible to undo this action. \n Are you sure you want to delete it?',
    hasCloseButton: true,
    hideFn: () => hideModal(),
}
}