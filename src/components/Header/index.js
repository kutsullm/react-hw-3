import React from 'react'
import PropTypes from "prop-types";
import { AiOutlineShoppingCart, AiOutlineHeart, AiOutlineHome } from "react-icons/ai"
import { StyledHeader } from "./styled";
import { Link } from 'react-router-dom';
import { useFav } from '../../context/FavContext';
import { useCart } from '../../context/CartContext';
export const Header = () => {
   
    const {fav} = useFav()
    const {cart} = useCart()
    const count = cart.reduce((acc, curr) => {
        acc += curr.counter
        return acc
    }, 0)
    return (
        <StyledHeader >
            <nav>
                <ul>
                    <li><Link to="/"><AiOutlineHome /></Link></li>
                     <li><Link to="/favorite"><AiOutlineHeart /> - {fav.length}</Link></li> 
                     <li><Link to="/cart"><AiOutlineShoppingCart /> - {count}</Link></li>  
                </ul>
            </nav>
        </StyledHeader>
    )
}
Header.propTypes = {
    favCount: PropTypes.number,
    cartCount: PropTypes.array,
}
