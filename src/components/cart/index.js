import React from 'react'
import Button from '../Button'
import { AiOutlineHeart, AiFillHeart } from 'react-icons/ai';
import { StyledImg } from "./styled";


export const Cart = ({ showModal, url, title, description, counter, id ,isInFav,toggleInFavorite}) => {

  return (
    <li> <p>{isInFav ? <AiFillHeart onClick={() => { toggleInFavorite(id) }} /> : <AiOutlineHeart onClick={() => { toggleInFavorite(id) }} />}</p> <h4>{title}</h4><StyledImg src={url} /><p>{counter}</p><p>{description}</p>
    <Button onClick={()=>showModal(id)} backgroundColor="alice-blue" text="delete" /></li>
  )
}
