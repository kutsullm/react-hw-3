
import Product from "../Product/"
import PropTypes from "prop-types";
const ProductList =(props)=>{
  
 
    const {products,showModal,toggleInFavorite} = props
   
    return(
       <> <ul>
         {products.map((product)=>{
          
        return <Product onClick={showModal} toggleInFavorite={toggleInFavorite} key = {product.id} {...product}/>
        
        })} 
    </ul>  </>
   )

}
ProductList.propTypes = {
  products:PropTypes.array,
  showModal:PropTypes.func,
  toggleInFavorite:PropTypes.func

}
export default ProductList