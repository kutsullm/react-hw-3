import { Route, Routes } from "react-router-dom"
import { HomePage } from "./pages/HomePage";
import { FavoritePage } from "./pages/FavoritePage";
import { CartPage } from "./pages/CartPage";
import { Header } from "./components/Header";
import { useState, useEffect } from "react";
import { useProduct } from "./context/ProductContext";
import { useFav } from "./context/FavContext";
import { useCart } from "./context/CartContext";



function App() {
  const { products, setProducts } = useProduct()
  const { fav, setFav } = useFav()
  const { cart, setCart } = useCart()
  const [favorite, setFavorite] = useState(fav)
  const [cartInStorage, setCartInStorage] = useState(cart)

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cartInStorage))
  }, [cartInStorage])

  useEffect(() => {
    localStorage.setItem("fav", JSON.stringify(favorite))
  }, [favorite])

  useEffect(() => {
    const getData = async () => {
      const res = await fetch("./data-base.json")
      const data = await res.json()

      const updatedData = updateData(data.productList)
      setProducts(updatedData)

    }
    getData()
  }, [])
  function updateData(products) {
    const newCart = cart.map((item) => item.id)
    const cartCheck = products.map((item) => {

      if (newCart.includes(item.id)) {

        item.isInCart = true
        return item
      }

      return item
    })

    const favCheck = cartCheck.map((item) => {
      if (fav.includes(item.id)) {
        item.isInFav = true
        return item
      }
      return item
    })
    return favCheck
  }
  return (
    <div className="">
      <Header favCount={0} cartCount={[]} />
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/cart" element={<CartPage />} />
        <Route path="/favorite" element={<FavoritePage />} />
      </Routes>
    </div>
  );
}

export default App;
